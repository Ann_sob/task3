﻿using System;
using System.Collections.Generic;
using System.Text;

namespace inheritance
{
    public class MathUtil : MathBase
    {
        public double GetNumberInPercent(int a, int b)
        {
            //return (((double)a / (double)b) * 100) - (double)a;
            int dev = Divide(a, b);
            int mul = Mul(dev, 100);
            int result = Min(mul, 100);
            return result;
        }
     }
  
}

