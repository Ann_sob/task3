﻿using System;

namespace inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            MathUtil mathUnit = new MathUtil();
            //Console.WriteLine();

            Console.WriteLine("Введите первое число");
            string str1 = Console.ReadLine();
            Console.WriteLine("Введите второе число");
            string str2 = Console.ReadLine();

            if (int.TryParse(str1, out int str1Result) && int.TryParse(str2, out int str2Result))
            {
                Console.WriteLine("Результат:");
                Console.WriteLine(mathUnit.Sum(str1Result, str2Result));
                Console.WriteLine(mathUnit.Min(str1Result, str2Result));
                Console.WriteLine(mathUnit.Mul(str1Result, str2Result));
                Console.WriteLine(mathUnit.Divide(str1Result, str2Result));
                Console.WriteLine($"{str1} more than {str2} by {mathUnit.GetNumberInPercent(str1Result, str2Result)} %");
            }
            else
            {
                Console.WriteLine("не число");
            }
        }

    }
}
