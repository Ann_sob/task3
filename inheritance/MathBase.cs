﻿using System;
using System.Collections.Generic;
using System.Text;

namespace inheritance
{
    public class MathBase
    {

        public int Sum(int a, int b) //int a, int b
        {
            return a + b;
        }

        public int Min(int a, int b) //int a, int b
        {
            return a - b;
        }

        public int Mul(int a, int b) //int a, int b
        {
            return a * b;
        }

        public int Divide(int a, int b) //int a, int b
        {
            return a / b;
        }
    }
}
